<?php

include 'datawritter.php';


class Student
{
    private $name,$lastname,$studentID,$grade;


    public function __construct($name,$lastname,$studentID,$grade)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->studentID = $studentID;
        $this->grade = $grade;
    }

    public function getName(){
        return $this->name;
    }

    public function getlastName(){
        return $this->lastname;
    }

    public function getStudentID(){
        return $this->studentID;
    }

    public function getGrade(){
        return $this->grade;
    }
}

if(array_key_exists('studentname',$_POST)){

$student = new Student($_POST['studentname'],$_POST['lastname'],$_POST['studentid'],$_POST['grade']);

$datawritter = new DataWriter();


$datawritter->writeStudentData($student);


}

header("Location: zadacastudents.php");

