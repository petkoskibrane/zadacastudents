<?php

interface PrintData
{
    public function PrintData($file);
}

class Printer implements PrintData
{
    public function printData($file)
    {
        $studentData = file_get_contents($file);
        $studentDataArray = explode(PHP_EOL,$studentData);

        $studentArray = [];

        foreach ($studentDataArray as $key => $value) {
            array_push($studentArray,explode(",",$value));
        }


        foreach ($studentArray as $key => $value) {
           echo $this->printArray($value);
            echo "<br>";
        }

        echo "<br>";
    }


    private function printArray($array){
        $string = "";
        if(count($array) == 4){
            $array[0] = "#".$array[0];
            $array[3] = $this->convertGrade($array[3]);
        }


        foreach ($array as $key => $value) {
            $string .= " ".$value;
        }


        return $string;


    }


    private function convertGrade($grade){
        $grades = [
            '6' => 'E',
            '7' => 'D',
            '8' => 'C',
            '9' => 'B',
            '10' => 'A'
        ];


        return $grades[$grade];
    }

}