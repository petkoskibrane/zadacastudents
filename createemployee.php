<?php

include 'datawritter.php';


class Employee
{
    private $name,$lastname,$salary;


    public function __construct($name,$lastname,$salary)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->salary = $salary;
    }

    public function getName(){
        return $this->name;
    }

    public function getLastName(){
        return $this->lastname;
    }

    public function getSalary(){
        return $this->salary;
    }
}



$datawritter = new DataWriter();

$employee = new Employee($_POST['name'],$_POST['lastname'],$_POST['salary']);

$datawritter->writeEmployeeData($employee);

// var_dump($employee);
header("Location: zadacastudents.php");


