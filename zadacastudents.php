<?php include 'dashboard.php';

?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <h1>Create student</h1>
        <form method="post" action="createstudent.php">
            <label> StudentID</label>
            <input type="number" name="studentid"/>
            <label> Name</label>
            <input type="text" name="studentname"/>
            <label> Last Name</label>
            <input type="text" name="lastname"/>
            <label> Grade</label>
            <input type="number" name="grade"/>

            <input type="submit" />
        </form>

        <h1>Create Employee</h1>
        <form method="post" action="createemployee.php">
            <label> Name</label>
            <input type="text" name="name"/>
            <label> Last Name</label>
            <input type="text" name="lastname"/>
            <label> Salary</label>
            <input type="number" name="salary"/>

            <input type="submit" />
        </form>
    
    </body>
</html>